package itc205_group8;

import java.util.Scanner;


public class BorrowBookUI {
	
	public static enum UIState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };

	private BorrowBookControl borrowBookControl;
	private Scanner scanner;
	private UIState state;

	
	public BorrowBookUI(BorrowBookControl control) {
		this.borrowBookControl = control;
		scanner = new Scanner(System.in);
		state = UIState.INITIALISED;
		control.setUI(this);
	}

	
	private String inputData(String prompt) {
		System.out.print(prompt);
		return scanner.nextLine();
	}	
		
		
	private void outputData(Object object) {
		System.out.println(object);
	}
	
			
	public void setState(UIState STATE) {
		this.state = STATE;
	}

	
	public void runBorrowBookFunction() {
		outputData("Borrow Book Use Case UI\n");
		
		while (true) {
			
			switch (state) {			
			
			case CANCELLED:
				outputData("Borrowing Cancelled");
				return;

				
			case READY:
				String memberString = inputData("Swipe member card (press <enter> to cancel): ");
				if (memberString.length() == 0) {
					borrowBookControl.cancel();
					break;
				}
				try {
					int memberId = Integer.valueOf(memberString).intValue();
					borrowBookControl.swipeCard(memberId);
				}
				catch (NumberFormatException e) {
					outputData("Invalid Member Id");
				}
				break;

				
			case RESTRICTED:
				inputData("Press <any key> to cancel");
				borrowBookControl.cancel();
				break;
			
				
			case SCANNING:
				String bookString = inputData("Scan Book (<enter> completes): ");
				if (bookString.length() == 0) {
					borrowBookControl.Complete();
					break;
				}
				try {
					int bookId = Integer.valueOf(bookString).intValue();
					borrowBookControl.Scanned(bookId);
					
				} catch (NumberFormatException e) {
					outputData("Invalid Book Id");
				} 
				break;
					
				
			case FINALISING:
				String answer = inputData("Commit loans? (Y/N): ");
				if (answer.toUpperCase().equals("N")) {
					borrowBookControl.cancel();
					
				} else {
					borrowBookControl.Commit_LOans();
					inputData("Press <any key> to complete ");
				}
				break;
				
				
			case COMPLETED:
				outputData("Borrowing Completed");
				return;
	
				
			default:
				outputData("Unhandled state");
				throw new RuntimeException("BorrowBookUI : unhandled state :" + state);			
			}
		}		
	}


	public void displayOutput(Object object) {
		outputData(object);		
	}


}
