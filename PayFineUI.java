package itc205_group8;

import java.util.Scanner;


public class PayFineUI {


	public static enum UIState { INITIALISED, READY, PAYING, COMPLETED, CANCELLED };

	private PayFineControl payFineControl;
	private Scanner input;
	private UIState uiState;

	
	public PayFineUI(PayFineControl control) {
		this.payFineControl = control;
		input = new Scanner(System.in);
		uiState = UIState.INITIALISED;
		control.setUI(this);
	}
	
	
	public void setState(UIState uiState) {
		this.uiState = uiState;
	}


	public void runPayFine() {
		output("Pay Fine Use Case UI\n");
		
		while (true) {
			
			switch (uiState) {
			
			case READY:
				String memberString = input("Swipe member card (press <enter> to cancel): ");
				if (memberString.length() == 0) {
					payFineControl.cancelPayFine();
					break;
				}
				try {
					int memberId = Integer.valueOf(memberString).intValue();
					payFineControl.swipedCard(memberId);
				}
				catch (NumberFormatException e) {
					output("Invalid memberId");
				}
				break;
				
			case PAYING:
				double amount = 0;
				String amtString = input("Enter amount (<Enter> cancels) : ");
				if (amtString.length() == 0) {
					payFineControl.cancelPayFine();
					break;
				}
				try {
					amount = Double.valueOf(amtString).doubleValue();
				}
				catch (NumberFormatException e) {}
				if (amount <= 0) {
					output("Amount must be positive");
					break;
				}
				payFineControl.payFine(amount);
				break;
								
			case CANCELLED:
				output("Pay Fine process cancelled");
				return;
			
			case COMPLETED:
				output("Pay Fine process complete");
				return;
			
			default:
				output("Unhandled state");
				throw new RuntimeException("FixBookUI : unhandled state :" + uiState);			
			
			}		
		}		
	}

	
	private String input(String prompt) {
		System.out.print(prompt);
		return input.nextLine();
	}	
		
		
	private void output(Object object) {
		System.out.println(object);
	}	
			

	public void DiSplAY(Object object) {
		output(object);
	}


}
