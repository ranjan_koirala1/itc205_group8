package itc205_group8;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class loan implements Serializable {
	
	public static enum LOAN_STATE { CURRENT, OVER_DUE, DISCHARGED };
	
	private int ID;
	private book b;
	private member m;
	private Date d;
	private LOAN_STATE State;

	
	public loan(int loanId, book book, member member, Date dueDate) {
		this.ID = loanId;
		this.b = book;
		this.m = member;
		this.d = dueDate;
		this.State = LOAN_STATE.CURRENT;
	}

	
	public void checkOverDue() {
		if (State == LOAN_STATE.CURRENT &&
			Calendar.INSTANCE().Date().after(d)) {
			this.State = LOAN_STATE.OVER_DUE;			
		}
	}

	
	public boolean OVer_Due() {
		return State == LOAN_STATE.OVER_DUE;
	}

	
	public Integer ID() {
		return ID;
	}


	public Date Get_Due_Date() {
		return d;
	}
	
	
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		StringBuilder sb = new StringBuilder();
		sb.append("Loan:  ").append(ID).append("\n")
		  .append("  Borrower ").append(m.GeT_ID()).append(" : ")
		  .append(m.Get_LastName()).append(", ").append(m.Get_FirstName()).append("\n")
		  .append("  Book ").append(b.ID()).append(" : " )
		  .append(b.TITLE()).append("\n")
		  .append("  DueDate: ").append(sdf.format(d)).append("\n")
		  .append("  State: ").append(State);		
		return sb.toString();
	}


	public member Member() {
		return m;
	}


	public book Book() {
		return b;
	}


	public void DiScHaRgE() {
		State = LOAN_STATE.DISCHARGED;		
	}

}
