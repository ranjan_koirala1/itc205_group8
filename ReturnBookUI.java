package itc205_group8;

import java.util.Scanner;


public class ReturnBookUI {

	public static enum UI_STATE { INITIALISED, READY, INSPECTING, COMPLETED }

	private ReturnBookControl returnBookControl;
	private Scanner scanner;
	private UI_STATE uistate;

	
	public ReturnBookUI(ReturnBookControl control) {
		this.returnBookControl = control;
		scanner = new Scanner(System.in);
		uistate = UI_STATE.INITIALISED;
		control.Set_UI(this);
	}


	public void runReturnBook() {		
		outputMessage("Return Book Use Case UI\n");
		
		while (true) {
			
			switch (uistate) {
			
			case INITIALISED:
				break;
				
			case READY:
				String bookString = inputData("Scan Book (<enter> completes): ");
				if (bookString.length() == 0) {
					returnBookControl.scanningComplete();
				}
				else {
					try {
						int bookId = Integer.valueOf(bookString).intValue();
						returnBookControl.Book_scanned(bookId);
					}
					catch (NumberFormatException e) {
						outputMessage("Invalid bookId");
					}					
				}
				break;				
				
			case INSPECTING:
				String answer = inputData("Is book damaged? (Y/N): ");
				boolean isDamaged = false;
				if (answer.toUpperCase().equals("Y")) {					
					isDamaged = true;
				}
				returnBookControl.dischargeLoan(isDamaged);
			
			case COMPLETED:
				outputMessage("Return processing complete");
				return;
			
			default:
				outputMessage("Unhandled state");
				throw new RuntimeException("ReturnBookUI : unhandled state :" + uistate);			
			}
		}
	}

	
	private String inputData(String prompt) {
		System.out.print(prompt);
		return scanner.nextLine();
	}	
		
		
	private void outputMessage(Object object) {
		System.out.println(object);
	}
	
			
	public void displayMessage(Object object) {
		outputMessage(object);
	}
	
	public void setState(UI_STATE state) {
		this.uistate = state;
	}

	
}
