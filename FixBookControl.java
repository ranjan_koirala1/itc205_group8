package itc205_group8;

public class FixBookControl {
	
	private FixBookUI UI;
	private enum CONTROL_STATE { INITIALISED, READY, FIXING };
	private CONTROL_STATE StAtE;
	
	private library LIB;
	private book Cur_Book;


	public FixBookControl() {
		this.LIB = LIB.INSTANCE();
		StAtE = CONTROL_STATE.INITIALISED;
	}
	
	
	public void setUI(FixBookUI ui) {
		if (!StAtE.equals(CONTROL_STATE.INITIALISED)) {
			throw new RuntimeException("FixBookControl: cannot call setUI except in INITIALISED state");
		}	
		this.UI = ui;
		ui.setState(FixBookUI.UIState.READY);
		StAtE = CONTROL_STATE.READY;		
	}


	public void bookScanned(int bookId) {
		if (!StAtE.equals(CONTROL_STATE.READY)) {
			throw new RuntimeException("FixBookControl: cannot call bookScanned except in READY state");
		}	
		Cur_Book = LIB.Book(bookId);
		
		if (Cur_Book == null) {
			UI.display("Invalid bookId");
			return;
		}
		if (!Cur_Book.IS_Damaged()) {
			UI.display("Book has not been damaged");
			return;
		}
		UI.display(Cur_Book.toString());
		UI.setState(FixBookUI.UIState.FIXING);
		StAtE = CONTROL_STATE.FIXING;		
	}


	public void fixBook(boolean MUST_fix) {
		if (!StAtE.equals(CONTROL_STATE.FIXING)) {
			throw new RuntimeException("FixBookControl: cannot call fixBook except in FIXING state");
		}	
		if (MUST_fix) {
			LIB.Repair_BOOK(Cur_Book);
		}
		Cur_Book = null;
		UI.setState(FixBookUI.UIState.READY);
		StAtE = CONTROL_STATE.READY;		
	}

	
	public void scanningComplete() {
		if (!StAtE.equals(CONTROL_STATE.READY)) {
			throw new RuntimeException("FixBookControl: cannot call scanningComplete except in READY state");
		}	
		UI.setState(FixBookUI.UIState.COMPLETED);		
	}






}
