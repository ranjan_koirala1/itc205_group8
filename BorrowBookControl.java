package itc205_group8;

import java.util.ArrayList;
import java.util.List;

public class BorrowBookControl {
	
	private BorrowBookUI UI;
	
	private library LIBRARY;
	private member M;
	private enum CONTROL_STATE { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };
	private CONTROL_STATE State;
	
	private List<book> PENDING;
	private List<loan> COMPLETED;
	private book BOOK;
	
	
	public BorrowBookControl() {
		this.LIBRARY = LIBRARY.INSTANCE();
		State = CONTROL_STATE.INITIALISED;
	}
	

	public void setUI(BorrowBookUI ui) {
		if (!State.equals(CONTROL_STATE.INITIALISED)) 
			throw new RuntimeException("BorrowBookControl: cannot call setUI except in INITIALISED state");
			
		this.UI = ui;
		ui.setState(BorrowBookUI.UIState.READY);
		State = CONTROL_STATE.READY;		
	}

		
	public void swipeCard(int MEMMER_ID) {
		if (!State.equals(CONTROL_STATE.READY)) 
			throw new RuntimeException("BorrowBookControl: cannot call cardSwiped except in READY state");
			
		M = LIBRARY.MEMBER(MEMMER_ID);
		if (M == null) {
			UI.displayOutput("Invalid memberId");
			return;
		}
		if (LIBRARY.MEMBER_CAN_BORROW(M)) {
			PENDING = new ArrayList<>();
			UI.setState(BorrowBookUI.UIState.SCANNING);
			State = CONTROL_STATE.SCANNING; }
		else 
		{
			UI.displayOutput("Member cannot borrow at this time");
			UI.setState(BorrowBookUI.UIState.RESTRICTED); }}
	
	
	public void Scanned(int bookId) {
		BOOK = null;
		if (!State.equals(CONTROL_STATE.SCANNING)) {
			throw new RuntimeException("BorrowBookControl: cannot call bookScanned except in SCANNING state");
		}	
		BOOK = LIBRARY.Book(bookId);
		if (BOOK == null) {
			UI.displayOutput("Invalid bookId");
			return;
		}
		if (!BOOK.AVAILABLE()) {
			UI.displayOutput("Book cannot be borrowed");
			return;
		}
		PENDING.add(BOOK);
		for (book B : PENDING) {
			UI.displayOutput(B.toString());
		}
		if (LIBRARY.Loans_Remaining_For_Member(M) - PENDING.size() == 0) {
			UI.displayOutput("Loan limit reached");
			Complete();
		}
	}
	
	
	public void Complete() {
		if (PENDING.size() == 0) {
			cancel();
		}
		else {
			UI.displayOutput("\nFinal Borrowing List");
			for (book B : PENDING) {
				UI.displayOutput(B.toString());
			}
			COMPLETED = new ArrayList<loan>();
			UI.setState(BorrowBookUI.UIState.FINALISING);
			State = CONTROL_STATE.FINALISING;
		}
	}


	public void Commit_LOans() {
		if (!State.equals(CONTROL_STATE.FINALISING)) {
			throw new RuntimeException("BorrowBookControl: cannot call commitLoans except in FINALISING state");
		}	
		for (book B : PENDING) {
			loan LOAN = LIBRARY.ISSUE_LAON(B, M);
			COMPLETED.add(LOAN);			
		}
		UI.displayOutput("Completed Loan Slip");
		for (loan LOAN : COMPLETED) {
			UI.displayOutput(LOAN.toString());
		}
		UI.setState(BorrowBookUI.UIState.COMPLETED);
		State = CONTROL_STATE.COMPLETED;
	}

	
	public void cancel() {
		UI.setState(BorrowBookUI.UIState.CANCELLED);
		State = CONTROL_STATE.CANCELLED;
	}
	
	
}
